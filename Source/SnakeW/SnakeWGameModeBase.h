// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeWGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEW_API ASnakeWGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASnakeWGameModeBase();

	//void OnFood();

	UPROPERTY(EditAnywhere, Category = "Game Rules")
	int32 FoodToWin;
};
