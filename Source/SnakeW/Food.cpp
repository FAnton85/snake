// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FoodComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	RootComponent = MeshComponent;

	// Create a particle system that we can activate or deactivate
	ParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("MovementParticles"));
	ParticleSystem->SetupAttachment(MeshComponent);
	ParticleSystem->bAutoActivate = true;
	ParticleSystem->SetRelativeLocation(FVector(-20.0f, 0.0f, 20.0f));
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			
						
		}
	}
}

void AFood::SpawnNewFood()
{
	float RandX = FMath::RandRange(Spawn_X_Min, Spawn_X_Max);
	float RandY = FMath::RandRange(Spawn_Y_Min, Spawn_Y_Max);
	FVector FoodSpawnPosition = FVector(RandX, RandY, Spawn_Z);
	FRotator FoodSpawnRotation = FRotator(0.0f, 0.0f, 0.0f);

	AFood* FoodActor1 = (AFood*) GetWorld()->SpawnActor(FoodActorClass, &FoodSpawnPosition, &FoodSpawnRotation);
	return;
	
//	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("FOOD!"));
}

