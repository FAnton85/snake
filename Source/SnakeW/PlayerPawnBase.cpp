// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
//#include "Components/WidgetComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
	
{
	
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	PawnCamera->ProjectionMode = ECameraProjectionMode::Orthographic;
	PawnCamera->OrthoWidth = 1024.0f;
	PawnCamera->AspectRatio = 3.0f / 4.0f;
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if(IsValid(SnakeActor))
	{
		if(value>0 && SnakeActor->LastMovedDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMovedDirection = EMovementDirection::UP;
		}
		else if(value<0 && SnakeActor->LastMovedDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMovedDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMovedDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMovedDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastMovedDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMovedDirection = EMovementDirection::LEFT;
		}
	}
}

