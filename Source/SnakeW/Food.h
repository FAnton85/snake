// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Particles/ParticleSystemComponent.h"
#include "Food.generated.h"

UCLASS()
class SNAKEW_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	// Food class to spawn.
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	// Mesh for food
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	// Spawn Coordinates Z-const, X,Y randon whithin the screen
	float Spawn_Z = 10.0f;
	UPROPERTY(EditAnywhere)
		float Spawn_X_Min;
	UPROPERTY(EditAnywhere)
		float Spawn_X_Max;
	UPROPERTY(EditAnywhere)
		float Spawn_Y_Min;
	UPROPERTY(EditAnywhere)
		float Spawn_Y_Max;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UParticleSystemComponent* ParticleSystem;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void SpawnNewFood();
};
