// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.0f;
	LastMovedDirection = EMovementDirection::LEFT;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(4);
	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for(int i=0; i< ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);		
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if(ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}

	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector;
	MovementVector.Z = 0;
	MovementVector.X = 0;
	MovementVector.Y = 0;
	switch (LastMovedDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	default:
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for(int i = SnakeElements.Num() - 1; i>0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlapedElement, AActor* Other)
{
	if(IsValid(OverlapedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlapedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
			
		if (MovementSpeed > 0.1f)
		{
			MovementSpeed = MovementSpeed - 0.05f;
			SetActorTickInterval(MovementSpeed);
			FString SpeedMsg = FString::Printf(TEXT("Speed : %s"), *FString::SanitizeFloat(MovementSpeed));
			GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Yellow, SpeedMsg);
		}

		else MovementSpeed = 0.5f;
		
			
	}
}

